# cells-coronita-theme

Your component description.

Example:
```html
<cells-coronita-theme></cells-coronita-theme>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-coronita-theme-scope      | scope description | default value  |
| --cells-coronita-theme  | empty mixin     | {}             |
