(function() {

  'use strict';

  Polymer({

    is: 'cells-coronita-theme',

    behaviors: [
      window.CellsBehaviors.i18nBehavior
    ]

  });

}());
